/*****************************************
 * Filename: Section01_DMADoubleTypeDef.cc 
 * Author: Matthew Morrison
 * E-mail: matt.morrison@nd.edu
 * 
 * This file demonstrates the purpose of dynamic 
 * memory allocation by allocating memory for an 
 * array of doubles input at the command line.
 * 
 * Modified version of Section01_DMADouble.cc, but the 
 * double* are replaced with a typedef double_ptr 
 * **************************************/

#include <iostream>
#include <stdlib.h> // atof for converting doubles.

// Type Definition for double pointer 
typedef double* double_ptr;

/**********************************
 * Function name: dynAllocDoubleArray
 * Preconditions: double_ptr*, int 
 * Postconditions: none
 * This function dynamically allocates a
 * double array of length of the integer
 * ********************************/
void dynAllocDoubleArray(double_ptr* d_ptr, int argc){
    *d_ptr = new double[argc-1];
}

/**********************************
 * Function name: dynDelDoubleArray
 * Preconditions: double_ptr*
 * Postconditions: none
 * This function dynamically allocates a
 * double array of length of the integer
 * ********************************/
void dynDelDoubleArray(double_ptr* d_ptr){
    free(*d_ptr);
}

/********************************
 *  Function: getDoubles
 *  Preconditions: double_ptr, int, int **
 *  Postconditions: none
 *  This function reads the values from the string array,
 *  and puts the doubles into the double array. Exits the
 *  program if one of the command line arguments is not valid
 * *****************************/
void getDoubles(double_ptr d_ptr, int argc, char **argv){
    for(int i = 1; i < argc; i++){
        if(d_ptr[i-1] = atof(argv[i]))
            ; // Do nothing
        else{
            std::cerr << "Error! The command-line input " << i << ": '" << argv[i] << "' is not a double" << std::endl;
            std::cerr << "Ending program" << std::endl;
            exit(-1);
        }
	}
}

/********************************
 *  Function: printDoubles
 *  Preconditions: double_ptr, int
 *  Postconditions: none
 *  This function prints the values of the doubles 
 *  in the Dynamically allocated array 
 * *****************************/
void printDoubles(double_ptr d_ptr, const int length){
    for(int i = 0; i < length; i++)
        std::cout << d_ptr[i] << " ";
    std::cout << std::endl;
}

/********************************
 *  Function: updateArray
 *  Preconditions: double_ptr, double, int, const double
 *  Postconditions: none
 *  This function updates the value of the double array at a given location, 
 *  and passes the length for error checking for array protection.
 * *****************************/
void updateArray(double_ptr d_ptr, double new_doub, int location, const double length){
    if(location >= 0 && location < length){
        d_ptr[location] = new_doub;
    }
    else{
        std::cerr << "Error! The location given " << location << " is outside the acceptable range of 0 to " << length - 1<< std::endl;
    }
}

/************************************
 * Function name: main
 * Preconditions: int, char **
 * Postconditions: int 
 * 
 * This is the main driver function
 * *********************************/
int main(int argc, char**argv)
{
    // Initialize the double array pointers
	double_ptr d_ptr;
	std::cout << "The *d_ptr at memory location " << &d_ptr << std::endl;
	std::cout << " is pointing to memory location " << d_ptr << std::endl;
	
    // The value in d_ptr will be 0, since it is not pointing to anything yet.
	
	// Dynamically allocate the double array with length argc
	dynAllocDoubleArray(&d_ptr, argc);
	std::cout << "After dynamic allocation, *d_ptr at memory location " << &d_ptr << " is pointing to memory location " << d_ptr << std::endl;
	// The output of d_ptr will now contain a memory address.
	
	// Get the doubles from the argv string array 
	getDoubles(d_ptr, argc, argv);
	
	// Print the array 
	printDoubles(d_ptr, argc-1);
	
	// Update the array
	double new_doub = 3.1415;
	
	// Test the range
	updateArray(d_ptr, new_doub, -5, argc-1);
	updateArray(d_ptr, new_doub, 7, argc-1);
	
	// Update using a correct value
	updateArray(d_ptr, new_doub, 2, argc-1);
	
	// Print the array 
	printDoubles(d_ptr, argc-1);
	
	// Dynamically delete the pointer
	dynDelDoubleArray(&d_ptr);

	return 0;
}